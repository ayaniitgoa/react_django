import React from 'react';
import { Form, Input, Button } from 'antd';
import axios from 'axios';
class CustomForm extends React.Component {
  handleFormSubmit = (event, requestType, articleID) => {
    const title = event.target.elements.title.value;
    const content = event.target.elements.content.value;

    switch (requestType) {
      case 'post':
        return axios
          .post('http://127.0.0.1:8000/api/', {
            title: title,
            content: content,
          })
          .then((res) => {
            console.log(res);
            window.location.reload();
          });
      case 'put':
        return axios
          .put(`http://127.0.0.1:8000/api/${articleID}/`, {
            title: title,
            content: content,
          })
          .then((res) => {
            console.log(res);
            window.location.reload();
          });
    }
  };
  render() {
    return (
      <div>
        <Form
          onSubmitCapture={(event) =>
            this.handleFormSubmit(
              event,
              this.props.requestType,
              this.props.articleID
            )
          }
          layout={'vertical'}
        >
          <Form.Item name='title' label='Title'>
            <Input placeholder='Title..' />
          </Form.Item>
          <Form.Item name='content' label='Content'>
            <Input placeholder='Content..' />
          </Form.Item>
          <Form.Item>
            <Button type='primary' htmlType='submit'>
              {this.props.btnText}
            </Button>
          </Form.Item>
        </Form>
      </div>
    );
  }
}

export default CustomForm;
