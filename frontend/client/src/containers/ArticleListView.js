import React from 'react';
import Article from '../components/article';
import axios from 'axios';
import CustomForm from '../components/form';
class ArticleList extends React.Component {
  state = {
    articles: [],
  };

  componentDidMount() {
    axios.get('http://127.0.0.1:8000/api/').then((res) => {
      this.setState({
        articles: res.data,
      });
    });
  }
  render() {
    return (
      <div className=''>
        <Article data={this.state.articles} />
        <h2>Create an Article</h2>
        <CustomForm requestType='post' articleID={null} btnText='Upload' />
      </div>
    );
  }
}

export default ArticleList;
