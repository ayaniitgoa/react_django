import React from 'react';
import { Route } from 'react-router-dom';
import ArticleList from './containers/ArticleListView';
import ArticleDetail from './containers/ArticleDetailView';
const BaseRouter = () => (
  <React.Fragment className=''>
    <Route exact path='/' component={ArticleList} />
    <Route exact path='/:articleID' component={ArticleDetail} />
  </React.Fragment>
);

export default BaseRouter;
